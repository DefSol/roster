﻿using Roster.Infrastucture.Massive;

namespace Roster.Entities
{
    public class Person : DynamicModel
    {
        public Person() : base("Roster", "Person", "ID") { }
    }

    public class Availability:DynamicModel
    {
        public Availability() :base("Roster","Available", "ID" ){}
    }
}