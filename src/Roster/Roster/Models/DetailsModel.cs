﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Roster.Models
{
    public class DetailsModel
    {
        [Required]
        [DisplayName("First Name")]        
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Mobile Phone")]
        public string MobilePhone { get; set; }

        [DisplayName("Facebook Account")]
        public string Facebook { get; set; }

        [DisplayName("Twitter Account")]
        public string Twitter { get; set; }

        [DisplayName("Google Account")]
        public string Google { get; set; }
    }
}