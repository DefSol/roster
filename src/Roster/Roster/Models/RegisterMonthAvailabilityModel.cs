﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace Roster.Models
{
    public class RegisterMonthAvailabilityModel : BaseModel
    {
         public RegisterMonthAvailabilityModel()
        {
            this.Sundays = new List<AvailableDate>();
        }


        /// <summary>
        /// The month for the roster
        /// </summary>
        /// <value>
        /// The month.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Get the firstt day of the month based on the Date       
        /// </summary>
        public DateTime? StartOfMonth
        {
            get
            {
                return new DateTime(Date.Year, Date.Month, 1);
            }
        }

        /// <summary>
        /// Get the last day of the month based on the Date       
        /// </summary>
        public DateTime? EndOfMonth
        {
            get
            {
                if (!StartOfMonth.HasValue)
                    return null;
                return StartOfMonth.Value.AddMonths(1).AddDays(-1);
            }
        }


        /// <summary>
        /// The list of Sundays for the Month
        /// </summary>
        /// <value>
        /// The sundays.
        /// </value>
        public List<AvailableDate> Sundays { get; set; }

        public void ListSundysInMonth(IEnumerable<dynamic> available = null)
        {                        
            var sundays = Enumerable.Range(1, DateTime.DaysInMonth(Date.Year, Date.Month))
                            .Select(day => new DateTime(Date.Year, Date.Month, day))
                            .Where(dt => dt.DayOfWeek == DayOfWeek.Sunday)
                            .ToList();
                        
            foreach (var sunday in sundays)
            {
                AvailableDate availableDate = null;
                DateTime sunday1 = sunday;
                if (available != null && available.Any(x => x.Date == sunday1))
                {
                    var expando = available.First(x => x.Date == sunday);
                    availableDate = new AvailableDate(expando.ID, expando.PersonID, expando.Date, expando.IsAvailable);
                }
                else
                    availableDate = new AvailableDate(null, PersonID.Value, sunday, true);
   
                Sundays.Add(availableDate);                                      
            }
        }

        public void Save()
        {
            foreach (var sunday in Sundays)
            {
            var available = new Entities.Availability();
                if (sunday.ID == null)                    
                    available.Insert(new {PersonID = PersonID, Date = sunday.Date, IsAvailable = sunday.IsAvailable});                                       
                else
                    available.Update(sunday, sunday.ID);
            }
        }

    }

    public class AvailableDate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableDate"/> class.
        /// Parameterless ctor
        /// </summary>
        public AvailableDate()
        {
            this.ID = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableDate"/> class.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="isAvailable">if set to <c>true</c> [is available].</param>
        public AvailableDate(int? ID, int personID, DateTime date, bool isAvailable = true)
        {
            this.ID = ID;
            this.PersonID = personID;
            this.Date = date;
            this.IsAvailable = isAvailable;
        }

        /// <summary>
        /// Gets or sets the person ID.
        /// </summary>
        /// <value>
        /// The person ID.
        /// </value>        
        public int? ID { get; set; }

        public int PersonID { get; set; }

        public DateTime Date { get; set; }

        public bool IsAvailable { get; set; }
    }
}