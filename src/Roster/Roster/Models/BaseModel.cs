﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace Roster.Models
{
    public class BaseModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int? PersonID
        {
            get
            {
                if (WebSecurity.CurrentUserId > 0)
                    return WebSecurity.CurrentUserId;

                return null;
            }
        }

        public string UserName
        { 
            get { return WebSecurity.CurrentUserName; }
        }
    }
}