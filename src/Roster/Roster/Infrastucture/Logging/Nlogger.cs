﻿using System;
using NLog;

namespace Roster.Infrastucture.Logging
{
    public class Nlogger : ILogger
    {
        private Logger _logger;

        public Nlogger()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Logs the info.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogInfo(string message)
        {
            _logger.Info(message);
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogWarning(string message)
        {
            _logger.Warn(message);
        }

        /// <summary>
        /// Logs the debug.
        /// </summary>
        /// <param name="message">The message.</param>
        public void LogDebug(string message)
        {
            _logger.Debug(message);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">string message.</param>
        public void LogError(string message)
        {
            _logger.Error(message);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="e">Takes the exception</param>
        public void LogError(Exception e)
        {
            _logger.Error(e);
        }

        /// <summary>
        /// Logs the fatal.
        /// </summary>
        /// <param name="message">String message.</param>
        public void LogFatal(string message)
        {
            _logger.Fatal(message);
        }

        /// <summary>
        /// Logs the fatal.
        /// </summary>
        /// <param name="e">Takes tehh exception</param>
        public void LogFatal(Exception e)
        {
            _logger.Fatal(e);
        }
        
    }
}