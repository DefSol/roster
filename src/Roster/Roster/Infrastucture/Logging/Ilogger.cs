using System;

namespace Roster.Infrastucture.Logging
{
    public interface ILogger
    {
        /// <summary>
        /// Logs the info.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogInfo(string message);

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogWarning(string message);

        /// <summary>
        /// Logs the debug.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogDebug(string message);

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">string message.</param>
        void LogError(string message);

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="e">Takes the exception</param>
        void LogError(Exception e);

        /// <summary>
        /// Logs the fatal.
        /// </summary>
        /// <param name="message">String message.</param>
        void LogFatal(string message);

        /// <summary>
        /// Logs the fatal.
        /// </summary>
        /// <param name="e">Takes tehh exception</param>
        void LogFatal(Exception e);
    }
}