﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Roster.Entities;
using Roster.Filters;
using Roster.Infrastucture.Logging;
using Roster.Models;
using WebMatrix.WebData;

namespace Roster.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]    
    public class HomeController : Controller
    {
        private ILogger _logger;

        public HomeController(ILogger logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Sets the persons availability for the month
        /// </summary>
        /// <param name="month">The month. If no month then will default to next month</param>
        /// <returns></returns>        
        public ActionResult RegisterAvailability(int? month)
        {
            var model = new RegisterMonthAvailabilityModel();
            //if no month set to next month for now
            if (!month.HasValue || month.Value < 1 || month.Value > 12)
                model.Date = DateTime.Today.AddMonths(1);
            else
                model.Date = new DateTime(DateTime.Today.Year, month.Value, 1);

            dynamic available = new Availability();

            var query = string.Format("Select * FROM Available WHERE PersonID = {0} AND Date BETWEEN '{1}' AND '{2}'",
                                        model.PersonID.Value, 
                                        model.StartOfMonth.Value.ToString("yyyy-MM-dd"), 
                                        model.EndOfMonth.Value.ToString("yyyy-MM-dd"));

            dynamic monthAvailable = available.Query(query);

            model.ListSundysInMonth(monthAvailable);                        

            return View(model);
        }

        [HttpPost]        
        public ActionResult RegisterAvailability(RegisterMonthAvailabilityModel model)
        {
            if (ModelState.IsValid)
            {                                               
                model.Save();
                return RedirectToAction("Index");
            }

            return View(model);

        }
    }
}
