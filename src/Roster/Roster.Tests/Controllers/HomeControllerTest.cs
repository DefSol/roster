﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roster;
using Roster.Controllers;
using Roster.Infrastucture.Logging;

namespace Roster.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {        
        [TestMethod]
        public void Index()
        {
            ILogger logger = new Nlogger();
            // Arrange
            HomeController controller = new HomeController(logger);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Modify this template to jump-start your ASP.NET MVC application.", result.ViewBag.Message);
        }

        [TestMethod]
        public void About()
        {
            ILogger logger = new Nlogger();

            // Arrange
            HomeController controller = new HomeController(logger);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Contact()
        {
            ILogger logger = new Nlogger();

            // Arrange
            HomeController controller = new HomeController(logger);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
